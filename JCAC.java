/**
 * Join Call Admission Control (JCAC) is a network resource 
 * management aimed at preserving a good QoS in a heterogeneous 
 * network.
 *
 * @author Ayaovi Espior Djissenou
 * @version 08/04/2016
 */

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
// 
// import java.util.List;
import java.util.ArrayList;


class JCAC
{
	static long startTime;
	
	public static void main(String args[])
	{
		int c1, c2, c3, t1, t2, t3, b1, b2, b3;
		c1 = 9; c2 = 6;
		t1 = 6; t2 = 6;
		b1 = 3; b2 = 3;
		long number_of_states = 0l;
		long iterations = 0l;

		ArrayList<String> admissibleStates = new ArrayList<String>();

		// tick();

		for (int m1 = 0; m1 <= (t1 / b1); ++m1)
		{
			for (int n1 = 0; n1 <= (c1 / b1); ++n1)
			{
				if ((m1 + n1) * b1 <= c1)
				{
					for (int m2 = 0; m2 <= (t2 / b2); ++m2)
					{
						for (int n2 = 0; n2 <= (c2 / b2); ++n2)
						{
							if ((m2 + n2) * b2 <= c2)
							{
								admissibleStates.add(m1+" "+n1+" "+m2+" "+n2);
								++number_of_states;
							}
							++iterations;
						}
					}
				}
				else
					++iterations;
			}
		}
		System.out.println("Number of iterations : " + iterations);
		System.out.println("Number of admissible states : " + number_of_states);

		int size = admissibleStates.size();
		int cutoff = 5;
		ForkJoinPool fjPool = new ForkJoinPool();

		// case G-1

		BlockedOrDroppedCalls obj = new BlockedOrDroppedCalls(admissibleStates, 0, size, cutoff, 1, 0);
		// tick();
		int result = fjPool.invoke(obj);
		System.out.println("Probability of blocing a new call from G-1 : " + result + " out of " + number_of_states);
		// float time = toc();
		obj = new BlockedOrDroppedCalls(admissibleStates, 0, size, cutoff, 1, 1);
		// tick();
		result = fjPool.invoke(obj);
		System.out.println("Probability of dropping a handoff call from G-1 : " + result + " out of " + number_of_states);

		// case G-2

		obj = new BlockedOrDroppedCalls(admissibleStates, 0, size, cutoff, 2, 0);
		// tick();
		result = fjPool.invoke(obj);
		System.out.println("Probability of blocing a new call from G-2 : " + result + " out of " + number_of_states);
		// float time = toc();
		obj = new BlockedOrDroppedCalls(admissibleStates, 0, size, cutoff, 2, 1);
		// tick();
		result = fjPool.invoke(obj);
		System.out.println("Probability of dropping a handoff call from G-2 : " + result + " out of " + number_of_states);
	}

	private static void tick() 
	{
		startTime = System.currentTimeMillis();
	}

	private static float toc() 
	{
		return (System.currentTimeMillis() - startTime); // 1000.0f;
	}
}

class BlockedOrDroppedCalls extends RecursiveTask<Integer>
{
	ArrayList<String> states;
	private int start;
	private int stop;
	private int cutoff;
	int group;
	int typeOfCall;
	int result;

	public BlockedOrDroppedCalls(ArrayList<String> states, int start, int stop, int cutoff, int group, int typeOfCall)
	{
		this.states = states;
		this.start = start;
		this.stop = stop;
		this.cutoff = cutoff;
		this.group = group;
		this.typeOfCall = typeOfCall;
		result = 0;
	}

	protected Integer compute()
	{
		if ((start - start) < cutoff)
		{
			String s[];
			for (int i = start; i < stop; ++i)
			{
				s = states.get(i).split(" ");	/* {m1 n1 m2 n2 m3 n3} */

				int m1 = new Integer(s[0]).intValue();
				int n1 = new Integer(s[1]).intValue();
				int m2 = new Integer(s[2]).intValue();
				int n2 = new Integer(s[3]).intValue();
				
				if (group == 1)
				{
					if (typeOfCall == 0)	/* i.e. new call */
					{
						if ((1 + m1 + n1) > 2)
							++result;
					}
					else if (typeOfCall == 1)	/* i.e. handoff call */
					{
						if ((1 + m1 + n1) > 3)
							++result;	
					}
				}

				else if (group == 2)
				{
					if (typeOfCall == 0)	/* i.e. new call */
					{
						if ((1 + m1 + n1) > 2 && (1 + m2 + n2) > 2)
							++result;
					}
					else if (typeOfCall == 1)	/* i.e. handoff call */
					{
						if ((1 + m1 + n1) > 3 && (1 + m2 + n2) > 2)
							++result;
					}	
				}
			}
			return result;
		}
		else 
		{
			BlockedOrDroppedCalls left = new BlockedOrDroppedCalls(states, start, (stop + start)/2, cutoff, group, typeOfCall);
			BlockedOrDroppedCalls right = new BlockedOrDroppedCalls(states, (stop + start)/2, stop, cutoff, group, typeOfCall);
			left.fork();
			return right.compute() + left.join();
		}
	}
}